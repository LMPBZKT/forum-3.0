// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  firebase: {
    projectId: 'forum-bfe38',
    appId: '1:715913710618:web:3110918f751952d34055a3',
    storageBucket: 'forum-bfe38.appspot.com',
    locationId: 'us-central',
    apiKey: 'AIzaSyBdGsPKYBsxZvswqPzNuanOd1u5-UiapUc',
    authDomain: 'forum-bfe38.firebaseapp.com',
    messagingSenderId: '715913710618',
    measurementId: 'G-N9VVFX2EWG',
  },
  production: false,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
